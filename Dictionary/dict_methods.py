# Methods in Dictionary

# 1. clear() -> Removes all the elements from the dictionary

print("\n========== clear() ==========\n")

car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}

print(car)                  # {'brand': 'Ford', 'model': 'Mustang', 'year': 1964}

car.clear()

print(car)                  # {}




# 2. copy() -> Returns a copy of the dictionary

print("\n========== copy() ==========\n")

car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}

x = car.copy()                  

print(x)                    # {'brand': 'Ford', 'model': 'Mustang', 'year': 1964}




# 3. fromkeys() ->  Returns a dictionary with the specified keys and the specified value.

# Parameters -> (keys, value)
# keys -> Required. An iterable specifying the keys of the new dictionary.
# value -> Optional. The value for all keys. Default value is None.

print("\n========== fromkeys() ==========\n")

# Create a dictionary having 3 keys of value 0
x = ('key1', 'key2', 'key3')
y = 0

thisdict = dict.fromkeys(x, y)

print(thisdict)                         # {'key1': 0, 'key2': 0, 'key3': 0}


# Creating a dictionary without using value.
x = ('key1', 'key2', 'key3')

thisdict = dict.fromkeys(x)

print(thisdict)                         # {'key1': None, 'key2': None, 'key3': None}




# 4. get() -> Returns the value of the specified key.

# Parameters -> (keyname, value)
# keyname -> Required. The keyname of the item you want to return the value from.
# value   -> Optional. A value to return if the specified key does not exist. Default value None

print("\n========== get() ==========\n")

car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}

x = car.get("model")

print(x)                        # Mustang


# Try to return the value of an item that do not exist.

car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}

x = car.get("price", 15000)
print(x)                        # 15000

y= car.get("resale")
print(y)                        # None




# 5. items() -> Returns a list containing a tuple for each key value pair.

print("\n========== items() ==========\n")

car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}

x = car.items()

print(x)                # dict_items([('brand', 'Ford'), ('model', 'Mustang'), ('year', 1964)])


# When an item in the dictionary changes value, the view object also gets updated.
car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}

x = car.items()

car["year"] = 2018

print(x)               # dict_items([('brand', 'Ford'), ('model', 'Mustang'), ('year', 2018)])




# 6. keys() -> Returns a list containing the dictionary's keys.

print("\n========== keys() ==========\n")

car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}

x = car.keys()

print(x)                    # dict_keys(['brand', 'model', 'year'])


# When an item is added in the dictionary, the view object also gets updated.
car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}

x = car.keys()

car["color"] = "white"

print(x)                    # dict_keys(['brand', 'model', 'year', 'color'])




# 7. pop() -> Removes the element with the specified key and returns the value of the removed item.

# Parameters -> (keyname, defaultvalue)
# keyname -> Required. The keyname of the item you want to remove.
# defaultvalue -> Optional. A value to return if the specified key do not exist. If this parameter is not specified, and the no item with the specified key is found, an error is raised.

print("\n========== pop() ==========\n")

car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}

x= car.pop("model")

print(car)                      # {'brand': 'Ford', 'year': 1964}

print(x)                        # Mustang




# 8. popitem() -> removes the item that was last inserted into the dictionary. In versions before 3.7, the popitem() method removes a random item. The removed item is the return value of the popitem() method, as a tuple.

print("\n========== popitem() ==========\n")

car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}

car['color']= 'red'

x= car.popitem()

print(car)                          # {'brand': 'Ford', 'model': 'Mustang', 'year': 1964}

print(x)                            # ('color', 'red')




# 9. setdefault() -> Returns the value of the specified key. If the key does not exist: insert the key, with the specified value.

# Parameters -> (keyname, value)
# keyname -> Required. The keyname of the item you want to return the value from.

# value -> Optional.
#       -> If the key exist, this parameter has no effect.
#       -> If the key does not exist, this value becomes the key's value
#       -> Default value None

print("\n========== setdefault() ==========\n")

car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}

x = car.setdefault("model", "Bronco")

print(x)        # Mustang


# Get the value of the "color" item, if the "color" item does not exist, insert "color" with the value "white".

car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}

x = car.setdefault("color", "white")

print(x)            # white




# 10. update() -> Updates the dictionary with the specified key-value pairs.

print("\n========== update() ==========\n")

car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}

car.update({"color": "White"})

print(car)                  # {'brand': 'Ford', 'model': 'Mustang', 'year': 1964, 'color': 'White'}




# 11. values() -> Returns a list of all the values in the dictionary.

print("\n========== values() ==========\n")

car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}

x = car.values()

print(x)                    # dict_values(['Ford', 'Mustang', 1964])


# When a value is changed in the dictionary, the view object also gets updated

car = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}

x = car.values()

car["year"] = 2018

print(x)                    # dict_values(['Ford', 'Mustang', 2018])