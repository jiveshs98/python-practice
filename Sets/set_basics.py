# Create a set of numbers

print("\n========== Set of Numbers ==========\n")

py_set_num = {3, 7, 11, 15}
print(py_set_num)               # {3, 11, 7, 15}

# Create a set of mixed data types

print("\n========== Set of Mixed Datatypes ==========\n")

py_set_mix = {11, 1.1, "11", (1, 2)}
print(py_set_mix)                       # {(1, 2), 1.1, 11, '11'}


# Set can't store duplicate elements
# It'll automatically filter the duplicates

print("\n========== Try Printing Set with Duplicate Elements ==========\n")

py_set_num = {3, 7, 11, 15, 3, 7}
print(py_set_num)                       # {11, 3, 15, 7}                    


# Set Constructor

print("\n========== Set Constructor ==========\n")

thisset = set(("apple", "banana", "cherry")) # note the double round-brackets
print(thisset)         # {'apple', 'cherry', 'banana'}

py_set= set([1,2,3,21,13,2,7,9,1])
print(py_set)           # {1, 2, 3, 7, 9, 13, 21}




# Create an empty set

print("\n========== Create Empty Set ==========\n")

# It would result in the creation of a dictionary object instead of creating a set.
# You can’t just use curly braces and expect a “Set” in return.
py_set_num = {}
print("The value of py_set_num:", py_set_num)
print("The type of py_set_num:", type(py_set_num))

print("\n")

# We will use the set() function but will not pass any argument to it.
# It will eventually return us an empty Set object.
py_set_num = set()
print("The value of py_set_num:", py_set_num)
print("The type of py_set_num:", type(py_set_num))


# Output
"""
The value of py_set_num: {}
The type of py_set_num: <class 'dict'>

The value of py_set_num: set()
The type of py_set_num: <class 'set'>
"""




# Set Membership

print("\n========== Set Membership ==========\n")

py_set= {1,2,'apple',2.3,'a'}

print(2 in py_set)          # True

print('apple' in py_set)    # True

print("c" in py_set)        # False

print(80 in py_set)         # False




# Access Set Elements
# It is not possible to access a specific element in a set.
# But we can retrieve all the elements.

print("\n========== Access Set Elements ==========\n")

fruits= {"apple","orange","mango"}

for item in fruits:
    print(item)


# Output (Not necessarily in the same order)
"""
mango
apple
orange
"""