"""

Frozen set is a unique type of set which is immutable 
and doesn’t allow changing its elements after assignment.

It supports all methods and operators as a set does, but those that don’t alter its content.

As you now know that the sets are mutable and thus become unhashable.

So, we can’t use them as keys for a Python dictionary.

On the contrary, the Frozen Set is by default hashable and can work as keys to a dictionary.

"""




"""

You can create a Frozen set with the help of the following function.

frozenset()

"""




"""

Also, the following Python methods can work with the Frozen set.

copy()
difference()
intersection()
isdisjoint()
issubset()
issuperset()
symmetric_difference()
union()


The methods which perform add or remove operations aren’t applicable for Frozen sets 
as they are immutable.

"""




# Set vs. Frozen set

print("\n========== Set vs Frozen Set ==========\n")

# A standard set
std_set = set(["apple", "mango","orange"])
 
# Adding an element to normal set is fine
std_set.add("banana")
 
print("Standard Set:", std_set)


# Output
"""
Standard Set: {'apple', 'mango', 'orange', 'banana'}
"""



print("\n")




# A frozen set
frozen_set = frozenset(["apple", "mango","orange"])
 
print("Frozen Set:", frozen_set)

# Output
"""
Frozen Set: frozenset({'apple', 'mango', 'orange'})
"""



# Below code will raise an error as we are modifying a frozen set
try:
    frozen_set.add("banana")
except Exception as ex:
    print("Error:", ex)


# Output
"""
Error: 'frozenset' object has no attribute 'add'
"""