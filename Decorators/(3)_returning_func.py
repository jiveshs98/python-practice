# Returning functions

def cool():

    print("I am cool!")
    
    def very_cool():
        return "I am very cool!"

    
    print("\nNow I am going to return a function.\n")

    return very_cool


myvar= cool()


# Output
"""
I am cool!

Now I am going to return a function.
"""


print(myvar)        # <function cool.<locals>.very_cool at 0x017E51D8>

print(myvar())      # I am very cool!