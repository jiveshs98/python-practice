def func():
    for i in range(3):
        yield i


g= func()

print(g)        # <generator object func at 0x002DB4C0>


print(next(g))      # 0

print(next(g))      # 1

print(next(g))      # 2

print(next(g))      # StopIteration Error



"""
After yielding all the values next() caused a StopIteration error. What this error informs us of is that all the values have been yielded.

We don’t get this error while using a for loop because a for loop automatically catches this error and stops calling next().
"""