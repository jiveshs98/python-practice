import os

print(__file__)

print(os.path.dirname(__file__))

with open("framework_and_api.txt") as file:
    content= file.read()

print(content)

print(range(0,11))    

lst1= ['a','b','c','d']
lst2= [1,2,3,4]

for i,j in zip(lst1,lst2):
    print(i+", "+str(j))

import random

lst= [1,2,3,4,5]
random.shuffle(lst)
print(lst)

for k in range(0,10):
    print(random.randint(0,100))

#inp= input("Enter something: ")
#print(inp)

def func():
    # This is a comment
    '''
    This function demonstrate the use of docstring in Python.
    '''
    print("Python docstrings are not comments.")

func()
print(func.__doc__)
