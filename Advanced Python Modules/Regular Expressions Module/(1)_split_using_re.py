# Splitting strings using regular expressions

import re

split_term= '@'

phrase= 'Nice to meet you, jiveshs98@gmail.com'

print(re.split(split_term, phrase))

# Output
"""
['Nice to meet you, jiveshs98', 'gmail.com']
"""