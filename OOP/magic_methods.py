# Implementing special methods.
# These methods are also known as Magic or Dunder methods.


class Book():

    def __init__(self,title,author,pages):
        self.title= title
        self.author= author
        self.pages= pages

    def __str__(self):
        return f"{self.title} by {self.author}"

    def __len__(self):
        return self.pages

    def __del__(self):
        print("A book object has been deleted.")

b= Book("Python Rocks", "Jose", 200)

# Before defining __str__
print(b)        # <__main__.Book object at 0x03A281D8>
print(str(b))   # <__main__.Book object at 0x03A281D8>


# After defining __str__
print(b)        # Python Rocks by Jose
print(str(b))   # Python Rocks by Jose




# Before defining __len__
print(len(b))                   # TypeError: object of type 'Book' has no len()


# After defining __len__
print(len(b))                   # 200




# Before defining __del__
del b
print(b)                        # NameError: name 'b' is not defined


# After defining __del__
del b                           # A book object has been deleted.
print(b)                        # NameError: name 'b' is not defined