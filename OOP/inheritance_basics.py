# Implementing Inheritance

# BASE CLASS
class Animal():

    def __init__(self):
        print("ANIMAL CREATED")

    def who_am_i(self):
        print("I am an animal.")

    def eat(self):
        print("I am eating.")


# DERIVED CLASS
class Dog(Animal):

    def __init__(self):
        Animal.__init__(self)
        print("DOG Created")

    def bark(self):
        print("WOOF!")

    # Method Overriding
    def who_am_i(self):
        print("I am a dog!")



mydog= Dog()
mydog.eat()
mydog.who_am_i()
mydog.bark()


# Output
"""
ANIMAL CREATED
DOG Created
I am eating.
I am a dog!
WOOF!
"""

