# String Methods

#1.  capitalize() – Returns the string with the first character capitalized and rest of the characters in lower case.

var = 'PYTHON'
print (var.capitalize())
# Output: Python

var = 'python'
print(var.capitalize())
# Output: Python



#2. lower() – Converts all the characters of the String to lowercase.

var = 'TechBeamers'
print (var.lower())
# Output: techbeamers



#3. upper() – Converts all the characters of the String to uppercase.

var = 'TechBeamers'
print (var.upper())
# Output: TECHBEAMERS



#4. swapcase() – Swaps the case of every character in the String means that lowercase characters got converted to uppercase and vice-versa.

var = 'TechBeamers'
print (var.swapcase())
# Output: tECHbEAMERS



#5. title() – Returns the ‘titlecased’ version of String, which means that all words start with uppercase and the rest of the characters in words are in lowercase.

var = 'welcome to Python programming'
print (var.title())
# Output: Welcome To Python Programming


#6. count( str, beg, end) – Returns the number of times substring ‘str’ occurs in the range [beg, end] if beg and end index are given else the search continues in full String Search is case-sensitive.

var='TechBeamers'
str='e'
print (var.count(str))
# Output: 3

var1='Eagle Eyes'
print (var1.count('e'))
# Output: 2

var2='Eagle Eyes'
print (var2.count('E',0,5))
# Output: 1


#7. islower() – Returns ‘True’ if all the characters in the String are in lowercase. If any of the char is in uppercase, it will return False.

var='Python'
print (var.islower())
# Output: False

var='python'
print (var.islower())
# Output: True


#8. isupper() – Returns ‘True’ if all the characters in the String are in uppercase. If any of the char is in lowercase, it will return False.

var='Python'
print (var.isupper())
# Output: False

var='PYTHON'
print (var.isupper())
# Output: True



#9 isdecimal() – Returns ‘True’ if all the characters in String are decimal. If any character in the String is of other data-type, it will return False. Decimal characters are those from the Unicode category Nd.

num=u'2016'

num2= "\u0030"  # Unicode for 0

num3=  "\u0047" # Unicode for G

print(num)
print (num.isdecimal())

print(num2)
print(num2.isdecimal())

print(num3)
print(num3.isdecimal())

# Output
# 2016
# True
# 0
# True
# G
# False



#10  isdigit() – Returns ‘True’ for any char for which isdecimal() would return ‘True and some characters in the ‘No’ category. If there are any characters other than these, it will return False’. Precisely, digits are the characters for which Unicode property includes: Numeric_Type=Digit or Numeric_Type=Decimal. For example, superscripts are digits, but fractions not.

print ('2'.isdigit())
# True

print ('²'.isdigit())
# True



#11 isnumeric() – Returns ‘True’ if all the characters of the Unicode String lie in any one of the categories Nd, No, and NI.If there are any characters other than these, it will return False. Precisely, Numeric characters are those for which Unicode property includes: Numeric_Type=Digit, Numeric_Type=Decimal or Numeric_Type=Numeric.

s = '1242323'
print(s.isnumeric())

#s = '²3455'
s = '\u00B23455'
print(s.isnumeric())

# s = '½'
s = '\u00BD'
print(s.isnumeric())

s = '1242323'
s='python12'
print(s.isnumeric())


# Output
# True
# True
# True
# False

num=u'2016'
print (num.isnumeric())
# Output: True

num=u'year2016'
print (num.isnumeric())
# Output: False


#12.  isalpha() – Returns ‘True’ if String contains at least one character (non-empty String), and all the characters are alphabetic, ‘False’ otherwise.

print ('python'.isalpha())
# Output: True

print ('python3'.isalpha())
# Output: False




#13. isalnum() – Returns ‘True’ if String contains at least one character (non-empty String), and all the characters are either alphabetic or decimal digits, ‘False’ otherwise.

print ('python'.isalnum())
# Output: True

print ('python3'.isalnum())
# Output: True



#14.  find(str [,i [,j]])  – Searches for ‘str’ in complete String (if i and j not defined) or in a sub-string of String (if i and j are defined).This function returns the index if ‘str’ is found else returns ‘-1’. Here, i=search starts from this index, j=search ends at this index.

# The find() method takes maximum of three parameters:
#   sub - It's the substring to be searched in the str string.
#   start and end (optional) - substring is searched within str[start:end]


# The find() method returns an integer value.
# If substring exists inside the string, it returns the index of first occurence of the substring.
# If substring doesn't exist inside the string, it returns -1.

var="Tech Beamers"
str="Beam"
print (var.find(str))
#Output: 5

var="Tech Beamers"
str="Beam"
print (var.find(str,4))
#Output: 5

var="Tech Beamers"
str="Beam"
print (var.find(str,7))
#Output: -1



#15.  index(str[,i [,j]])  – This is same as ‘find’ method. The only difference is that it raises the ‘ValueError’ exception if ‘str’ doesn’t exist.

var='Tech Beamers'
str='Beam'
print (var.index(str))
# Output: 5

var='Tech Beamers'
str='Beam'
print (var.index(str,4))
# Output: 5

var='Tech Beamers'
str='Beam'
# print (var.index(str,7))
# Output: ValueError: substring not found



#16. rfind(str[,i [,j]]) – This is same as find() just that this function returns the last index where ‘str’ is found. If ‘str’ is not found, it returns ‘-1’.

var='This is a good example'
str='is'

print (var.rfind(str,0,10))
# Output: 5

print (var.rfind(str,10))
# Output: -1



#17. count(str[,i [,j]]) – Returns the number of occurrences of substring ‘str’ in the String. Searches for ‘str’ in the complete String (if i and j not defined) or in a sub-string of String (if i and j are defined). Where: i=search starts from this index, j=search ends at this index.


var='This is a good example'
str='is'
print (var.count(str))
#Output: 2

print (var.count(str,4,10))
#Output: 1


#18. replace(old,new[,count]) – Replaces all the occurrences of substring ‘old’ with ‘new’ in the String.If the count is available, then only ‘count’ number of occurrences of ‘old’ will be replaced with the ‘new’ var. Where old =substring to replace, new =substring


var='This is a good example'
str='was'
print (var.replace('is',str))
print (var.replace('is',str,1))

#Output: Thwas was a good example
#Output: Thwas is a good example



#19.  split([sep[,maxsplit]]) – Returns a list of substring obtained after splitting the String with ‘sep’ as a delimiter. Where, sep= delimiter, the default is space, maxsplit= number of splits to be done.

var = "This is a good example"
print (var.split())
print (var.split(' ', 3))
#Output: ['This', 'is', 'a', 'good', 'example']
#Output: ['This', 'is', 'a', 'good example']


#20. splitlines(num) – Splits the String at line breaks and returns the list after removing the line breaks. Where num = if this is a positive value. It indicates that line breaks will appear in the returned list.

var='Print new line\nNextline\n\nMove again to new line'
print (var.splitlines())
print (var.splitlines(1))
#Output: ['Print new line', 'Nextline', '', 'Move again to new line']
#Output: ['Print new line\n', 'Nextline\n', '\n', 'Move again to new line']



#21. join(seq) – Returns a String obtained after concatenating the sequence ‘seq’ with a delimiter string. Where: the seq= sequence of elements to join.

seq=('ab','bc','cd')
str='='
print (str.join(seq))
#Output: ab=bc=cd



#22. lstrip([chars]) – Returns a string after removing the characters from the beginning of the String. Where: Chars=this is the character to be trimmed from the String. The default is whitespace character.

var=' This is a good example '
print (var.lstrip())
# Output: This is a good example
var='*****This is a good example*****'
print (var.lstrip('*'))
# Output: This is a good example**********




#23. rstrip() – Returns a string after removing the characters from the End of the String. Where: Chars=this is the character to be trimmed from the String. The default is whitespace character.

var=' This is a good example '
print (var.rstrip())
# Output: This is a good example
var='*****This is a good example*****'
print (var.rstrip('*'))
# Output: *****This is a good example



#24. rindex(str[,i [,j]]) – Searches for ‘str’ in the complete String (if i and j not defined) or in a sub-string of String (if i and j are defined). This function returns the last index where ‘str’ is available. If ‘str’ is not there, then it raises a ValueError exception. Where: i=search starts from this index, j=search ends at this index.

var='This is a good example'
str='is'
print (var.rindex(str,0,10))
# Output: 5
# print (var.rindex(str,10))
# Output: ValueError: substring not found



#25. len(string) – Returns the length of given String
var='This is a good example'
print (len(var))
# Output: 22