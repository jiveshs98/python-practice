# if-elif-else Statement

print("\n========== if-elif-else ===========\n")

a= 200
b= 33

if b>a:
    print("b is greater")

elif a==b:
    print("a and b are equal")

else:
    print("a is greater")


# Output
# a is greater




print("\n========== if-else ==========\n")

a= 200
b=33

if b>a:
    print("b is greater than a")

else:
    print("b is not greater than a")


# Output
# b is not greater than a





# Ternary Operator

print("\n========== Ternary Operator ==========\n")

# Example 1
a= 2
b= 330

print("A") if a>b else print("B")

# Output
# B


# Example 2
a=b=330

print("A") if a>b else print("=") if a==b else print("B")

# Output
# =






# While loop

print("\n========== While Loop ==========\n")

i= 1

while i<6:
    print(i)
    i+=1

else:
    print("i is no longer less than 6")


# Output
"""
1
2
3
4
5
i is no longer less than 6
"""





# For Loop

print("\n========== For Loop ==========\n")

# Example 1
for x in range(6):
    print(x)

# Output
"""
0
1
2
3
4
5
"""


print("\n")


# Example 2
for i in range(2,6):
    print(i)


# Output
"""
2
3
4
5
"""


print("\n")



# Example 3
for x in [0,1,2]:
    print(x)


# Output
"""
0
1
2
"""


print("\n")


d= {'k1':1, 'k2':2, 'k3':3}

for k,v in d.items():
    print(k)
    print(v)


# Output
"""
k1
1
k2
2
k3
3
"""