# Pig Latin

"""
1. If word starts with a vowel, add 'ay' to it.
2. If word does not starts with a vowel, put first letter at the end, then add 'ay'.
3. word --> ordway
4. apple --> appleay
"""


def pig_latin(word):

    first_letter= word[0]

    # check if vowel

    if first_letter in 'aeiou':
        pig_word = word + 'ay'

    else:
        pig_word = word[1:] + first_letter + 'ay'

    return pig_word


print(pig_latin("word"))        # ordway

print(pig_latin("apple"))       # appleay