# 1-D List

print("\n========== 1-D List ==========\n")

lst= [0] * 3
print(lst)          # [0, 0, 0]




# 2-D List

print("\n========== 2-D List ==========\n")

two_dim_lst= [ [0] * 3 ] * 3

print(two_dim_lst)              # [[0, 0, 0], [0, 0, 0], [0, 0, 0]]




#====================================================

print("\n========== Without List Comprehension ==========\n")

two_dim_lst[0][2]= 1
print(two_dim_lst)                      # [[0, 0, 1], [0, 0, 1], [0, 0, 1]]


"""

Problem!!!!

Here, we wanted to change the value of 3rd item in the 1st row. 
But the same column in other rows got affected too.


Solution:

Use list comprehension to create multi-dimensional lists

"""



print("\n========== With List Comprehension ==========\n")

two_dim_lst= [ [0]*3 for i in range(3) ]

print("Original List: \n")
print(two_dim_lst)                      # [[0, 0, 0], [0, 0, 0], [0, 0, 0]]

two_dim_lst[0][2]= 1

print("\nModified List: \n")
print(two_dim_lst)                      # [[0, 0, 1], [0, 0, 0], [0, 0, 0]]