# Example 1

print("\n========== Example 1 ==========\n")

lst= [x for x in 'word']
print(lst)                      # ['w', 'o', 'r', 'd']




# Example 2

print("\n========== Example 2 ==========\n")

lst= [x**2 for x in range(0,11)]
print(lst)                          # [0, 1, 4, 9, 16, 25, 36, 49, 64, 81, 100]




# Using if statement in list comprehension
# Check for even no.s in a range

print("\n========== Using if statement in list comprehension ==========\n")

lst= [x for x in range(11) if x%2==0]
print(lst)                              # [0, 2, 4, 6, 8, 10]




# Using if-else statement in list comprehension

print("\n========== Using if-else statement in list comprehension ==========\n")

lst= [1,2,3,4,5]
res= ["even" if x%2 == 0 else "odd" for x in lst]
print(res)                                          # ['odd', 'even', 'odd', 'even', 'odd']




# Convert Celsius to Fahrenheit

print("\n========== Celsius to Fahrenheit ==========\n")

celsius= [0,10,20.1,34.5]

fahrenheit= [ ( (9/5) * temp + 32 ) for temp in celsius ]

print(fahrenheit)               # [32.0, 50.0, 68.18, 94.1]


# Nested List Comprehension

print("\n========== Nested List Comprehension ==========\n")

lst= [ x**2 for x in [x**2 for x in range(11)] ]

print(lst)                      # [0, 1, 16, 81, 256, 625, 1296, 2401, 4096, 6561, 10000]