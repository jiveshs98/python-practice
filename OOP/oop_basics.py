# Example 1

class SampleClass():
    pass


my_object= SampleClass()

print(type(my_object))              # <class '__main__.SampleClass'>




print("\n")




# Example 2

class Dog():

    # Constructor    
    def __init__(self, mybreed):

        # Attributes
        # We assign the parameters to self.attribute_name
        self.breed= mybreed


my_dog= Dog(mybreed= 'Huskie')
print(type(my_dog))             # <class '__main__.Dog'>
print(my_dog.breed)             # Huskie

my_dog2= Dog('Pitbull')
print(my_dog2.breed)            # Pitbull




print("\n")




class DogClass():

    def __init__(self, breed, name, spots):

        self.breed= breed
        self.name= name
        self.spots= spots

dog_obj= DogClass('Pug',"Edgar","NO SPOTS")

print(dog_obj.breed)
print(dog_obj.name)
print(dog_obj.spots)


# Output
"""
Pug
Edgar
NO SPOTS
"""