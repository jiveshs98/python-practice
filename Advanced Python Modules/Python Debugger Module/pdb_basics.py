"""
The pdb module implements an interactive debugging environment for Python programs.

It includes features to let you pause your program, look at the values of variables, and watch program execution step-by-step, so you can understand what your program actually does and find bugs in the logic.

Let's implement a set_trace() using the pdb module. This will allow us to basically pause the code at the point of the trace and check if anything is wrong.

This will allow us to check what the various variables were and check for errors.

Use 'q' to quit the debugger.
"""

# Without Debugger

x= [1,2,3]

y=2

z= 3

res1= y+z
print(res1)


try:
    res2= y+x               # TypeError
    print(res2)

except TypeError:
    print("TypeError: unsupported operand type(s) for +: 'int' and 'list'")


# Implementing Debugger
import pdb

x = [1,3,4]
y = 2
z = 3

result = y + z
print(result)

# Set a trace using Python Debugger
pdb.set_trace()

result2 = y+x
print(result2)