# Function to capitalize a given string

def cap_text(text):
    '''
    Input a string.
    Output the capitalized string.
    '''
    return text.title()