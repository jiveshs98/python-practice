"""
We can use StringIO to turn normal strings into in-memory file objects in our code. 

This kind of action has various use cases, especially in web scraping cases where you want to read some string you scraped as a file.

Text data is stored in a StringIO object, while binary data would be stored in a BytesIO object. 

This object can then be used as input or output to most functions that would expect a standard file object.
"""

import io

# Arbitrary String
message = 'This is just a normal string.'

# Use StringIO method to set as file object
f = io.StringIO(message)

# Now we have an object f that we will be able to treat just like a file.
print(f.read())             # This is just a normal string.

# We can also write to it
f.write(' Second line written to file like object')

# Reset cursor just like you would a file
f.seek(0)

# Read again
print(f.read())     # This is just a normal string. Second line written to file like object

# Close the object when contents are no longer needed
f.close()