# Generate a fibonacci series

def fibo(n):

    a= 1
    b= 1

    for i in range(n):
        yield a
        a,b = b, a+b

for number in fibo(10):
    print(number)


# Output
"""
1
1
2
3
5
8
13
21
34
55
"""