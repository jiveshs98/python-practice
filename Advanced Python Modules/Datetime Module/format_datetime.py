# 1. strftime() --> This method creates a formatted string from a given date, datetime or time object.

print("\n========= Using strftime() ==========\n")

import datetime

x = datetime.datetime(2018,6,1)

print(x.strftime("%B"))                         # June




# 2. strptime() --> It creates a datetime object from a given string. You cannot create a datetime object from every string. The string needs to be in a certain format.


from datetime import datetime

date_string = "21 June, 2018"
print(date_string)                      # 21 June, 2018
print(type(date_string))                # <class 'str'>


print("\n")


date_object = datetime.strptime(date_string, "%d %B, %Y")

print(date_object)                      # 2018-06-21 00:00:00
print(type(date_object))                # <class 'datetime.datetime'>