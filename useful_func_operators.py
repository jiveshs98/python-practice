# Useful Functions and Operators

# 1. range()

print("\n========== range() ==========\n")


print(range(0,11))              # range(0, 11)

print(list(range(0,11)))        # [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

print(list(range(0,11,2)))      # [0, 2, 4, 6, 8, 10]





# 2. enumerate()

print("\n========== enumerate() ==========\n")


print(enumerate('abcde'))               # <enumerate object at 0x038458C8>

print(list(enumerate('abcde')))         # [(0, 'a'), (1, 'b'), (2, 'c'), (3, 'd'), (4, 'e')]

print(tuple(enumerate('abc')))          # ((0, 'a'), (1, 'b'), (2, 'c'))

print(set(enumerate('abc')))            # {(1, 'b'), (2, 'c'), (0, 'a')}


print("\n")


# We can also pass a specific index as a parameter from which we want to start the counter.

lst= ["Python","Programming","Is","Fun"]

# Counter starts from 0
enum= enumerate(lst)
print(list(enum))                       # [(0, 'Python'), (1, 'Programming'), (2, 'Is'), (3, 'Fun')]

# Counter value starts from 5
enum= enumerate(lst, 5)
print(list(enum))                       # [(5, 'Python'), (6, 'Programming'), (7, 'Is'), (8, 'Fun')]


print("\n")


# Looping over an enumerate object

for i in enumerate(lst):
    print(i)


# Output
"""
(0, 'Python')
(1, 'Programming')
(2, 'Is')
(3, 'Fun')
"""


print("\n")


for i in enumerate(lst, 10):
    print(i)


# Output
"""
(10, 'Python')
(11, 'Programming')
(12, 'Is')
(13, 'Fun')
"""





# 3. zip()

print("\n========== zip() ==========\n")

a= [1,2,3,4]
b= [5,6,7,8]

c= zip(a,b)
print(c)                    # <zip object at 0x02E159E8>
print(list(c))              # [(1, 5), (2, 6), (3, 7), (4, 8)]


print("\n")


# If the sequence used in the zip function is unequal, the returned list is truncated in length to the length of the shortest sequence.

a= [1,2]
b= [5,6,7,8]

c= zip(a,b)
print(list(c))              # [(1, 5), (2, 6)]


print("\n")


# zip() is implemented in for loop as:-

lst1= ['a','b','c','d']
lst2= [1,2,3,4]

for i,j in zip(lst1, lst2):
    print(i + "," + str(j))


# Output
"""
a,1
b,2
c,3
d,4
"""




# 4. in operator

print("\n========== in operator ==========\n")

print('x' in ['x','y','z'])         # True

print('x' in [1,2,3])               # False





# 5. min & max

print("\n========== min & max ==========\n")

mylist= [10,20,30,40,100]

print(min(mylist))                  # 10
print(max(mylist))                  # 100






# 6. random module

print("\n========== random module ==========\n")

import random

lst= [1,2,3,4,5]
random.shuffle(lst)
print(lst)                      # [4, 5, 3, 2, 1]


print("\n")


from random import randint

for i in range(0, 10):
    print(randint(0,100))


# Output
"""
74
28
57
84
12
21
73
7
59
70
"""





# 7. input()

print("\n========== input() ==========\n")

inp= input("Enter Something: ")

print(inp)          # ad