# Example 1 (Without global statement)

x = 50

def func(x):
    print('x is', x)
    x = 2
    print('Changed local x to', x)

func(x)
print('x is still', x)


# Output
"""
x is 50
Changed local x to 2
x is still 50
"""





# Example 2 (With global statement)

x = 50

def func2():
    
    global x
    
    print('Because of global, x is: ', x)
    x = 2    
    print('Changed global x to', x)


print('Before calling func2(), x is: ', x)
func2()
print('Value of x after running func2(): ', x)


# Output
"""
Before calling func2(), x is:  50
Because of global, x is:  50
Changed global x to 2
Value of x after running func2():  2
"""