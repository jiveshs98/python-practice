# Tuple Indexing

print("\n========== Tuple Indexing ==========\n")

thistuple= ("apple","banana","cherry")

# Print the Second Item in the tuple
print(thistuple[1])                 # banana

# Print the last item of the tuple
print(thistuple[-1])                # cherry




# Tuple Slicing

print("\n========== Tuple Slicing ==========\n")

thistuple= ("apple", "banana", "cherry", "orange", "kiwi", "melon", "mango")

# Return third, fourth and fifth item
print(thistuple[2:5])                   # ('cherry', 'orange', 'kiwi')

# Return third, fourth and fifth item using negative indices
print(thistuple[-5:-2])                  # ('cherry', 'orange', 'kiwi')