# List constructor

print("\n========== List Constructor==========\n")

l= list()   # Empty list
print(len(l))   # 0

l1= list((1,2,3,4))
print(l1)       # [1,2,3,4]

# ======================================================

# Concatenating list (+)

print("\n========== Concatenating List (+) ==========\n")

mylist= ['a','b','c']
print(mylist+l1)    # ['a','b','c',1,2,3,4]

# However, there is no change in the original list
print(mylist)       #['a','b','c']

# Re-assign the original list to change
mylist= mylist + l1
print(mylist)       # ['a','b','c',1,2,3,4]


mylist= [1,2,3,4]
mylist2= [5,6,7,8]
print(mylist + mylist2)     #[1,2,3,4,5,6,7,8]

# =======================================================

#  Repeating lists (*)

print("\n========== Repeating Lists (*) ==========\n")

mylist= ['a','b','c']
print(mylist*2)    # ['a','b','c','a','b','c']

# However, there is no change in the original list
print(mylist)       #['a','b','c']

# Re-assign the original list to change
mylist= mylist * 2
print(mylist)       # ['a','b','c','a','b','c']


mylist= [1,2,3,4]
print(mylist * 2)   # [1,2,3,4,1,2,3,4]

# ===============================================================

# List Membership Test

print("\n\n\n========== List Membership ==========\n")

lst= ['p','r','o','b','l','e','m']

print('p' in lst)                       # True
print('a' in lst)                       # False
print('c' not in lst)                   # True




# Using 'del' Keyword

print("\n========== Using 'del' Keyword ==========\n")

# Remove an item from a specified index of a list
thislist= ["apple","banana","cherry"]
print("List before deletion: ",thislist)

del thislist[0]       # Remove from index 0      

print("\nList after deletion: ", thislist)
print(thislist[0])

print("\n")

# Delete entire list
thislist= ["apple","banana"]
print("List before deletion: ", thislist)

del thislist           # Remove list

print("\nPrinting List after deletion...\n")

try:
    print(thislist)

except NameError:
    print("NameError: List has been deleted successfully!!")




# Output
"""
List before deletion:  ['apple', 'banana', 'cherry']

List after deletion:  ['banana', 'cherry']
banana


List before deletion:  ['apple', 'banana']

Printing List after deletion...

NameError: List has been deleted successfully!!
"""