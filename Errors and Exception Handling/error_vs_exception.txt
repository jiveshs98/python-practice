Python Exception Handling: Error vs. Exception

What is Error?

The error is something that goes wrong in the program, e.g., like a syntactical error.

It occurs at compile time. Let’s see an example.

if a<5
File "<interactive input>", line 1
    if a < 5
           ^
SyntaxError: invalid syntax



What is Exception?

The errors also occur at runtime, and we know them as exceptions. An exception is an event which occurs during the execution of a program and disrupts the normal flow of the program’s instructions.

In general, when a Python script encounters an error situation that it can’t cope with, it raises an exception.

When a Python script raises an exception, it creates an exception object.

Usually, the script handles the exception immediately. If it doesn’t do so, then the program will terminate and print a traceback to the error along with its whereabouts.

>>> 1 / 0
Traceback (most recent call last):
 File "<string>", line 301, in run code
 File "<interactive input>", line 1, in <module>
ZeroDivisionError: division by zero