# Syntax for defining a function


def name_of_function(arg1,arg2):
    '''
    This is where the function's Document String (docstring) goes
    '''
    # Do stuff here
    # Return desired result






# Example 1

def hello_func(name):
    '''
    Docstring for hello_func():-

        Input: string containing name

        Output: Hello name
    '''

    print(f"Hello {name}")


hello_func('Jivesh')                    # Hello Jivesh



print("\n")







# Example 2

def dog_check(mystring):
    '''
    Function to check 'dog' in the given string.
    '''

    return 'dog' in mystring.lower()


print(dog_check.__doc__)

input_string1= "His dog is healthy."
print(dog_check(input_string1))                     # True

input_string2= "Dogs are faithful animals."
print(dog_check(input_string2))                     # True

input_string3= "They come in different breeds."
print(dog_check(input_string3))                     # False
