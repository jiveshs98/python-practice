# Implementing lambda expressions (Also known as anonymous function)


# A lambda function is a small anonymous function.

# A lambda function can take any number of arguments, but can only have one expression.

# Syntax ---> lambda arguments : expression

# The expression is executed and the result is returned.


square= lambda num : num**2

print(square(2))            # 4

print(square(5))            # 5



print("\n")



# Using lambda with map function

mynums= [1,2,3]

print(list(map(lambda num : num ** 2, mynums)))       # [1, 4, 9]



print("\n")



# Using lambda with filter function

lst_num= [1,2,3,4,5,6]

print(list(filter(lambda num : num%2 == 0, lst_num)))       # [2, 4, 6]



print("\n")



# Using lambda with strings

names= ["Jivesh","Kapil","Akshay"]

# Print Only First Character
print(list(map(lambda x : x[0], names)))            # ['J', 'K', 'A']

# Reverse the names
print(list(map(lambda x : x[::-1], names)))         # ['hseviJ', 'lipaK', 'yahskA']