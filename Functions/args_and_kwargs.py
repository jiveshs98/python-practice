# In Python, we can pass a variable number of arguments to a function using special symbols. There are two special symbols:

# *args (Non Keyword Arguments)
# **kwargs (Keyword Arguments)

# We use *args and **kwargs as an argument when we are unsure about the number of arguments to pass in the functions.


# 1. *args


# Python has *args which allow us to pass the variable number of non keyword arguments to function.

# In the function, we should use an asterisk * before the parameter name to pass variable length arguments.
# The arguments are passed as a tuple. 
# These passed arguments make tuple inside the function with same name as the parameter excluding asterisk *.



print("\n========== Using *args ===========\n")


# Example 1
# Calculate 5% of the sum of given numbers.

def five_sum(*args):
    print(type(args))
    return sum(args)*.05

result= five_sum(40,60,20)
print(result)               # 6.0



print("\n")



# Example 2
# Define a function called myfunc that takes in an arbitrary number of arguments, and returns the sum of those arguments.

def myfunc(*args):
    return sum(args)

result= myfunc(1,2,3,4)
print(result)                   # 10



print("\n")



# It is worth noting that the word "args" is itself arbitrary - any word will do so long as it's preceded by an asterisk.

# Example 3
def myfunc2(*numbers):
    return sum(numbers)

result= myfunc2(1,2,3)
print(result)                   # 6


print("\n")


# Example 4
# Define a function that takes an arbitrary no. of arguments, and returns a list containing only those arguments that are even.

def show_even(*args):
    return [x for x in args if x%2 == 0]

result= show_even(1,2,3,4,5,6,7,8,9,10)
print(result)                               # [2, 4, 6, 8, 10]




print("\n")



# 2. **kwargs

print("\n========== Using kwargs ==========\n")


# Similarly, Python offers a way to handle arbitrary numbers of keyworded arguments.

# Instead of creating a tuple of values, **kwargs builds a dictionary of key/value pairs.

# In the function, we use the double asterisk ** before the parameter name to denote this type of argument. 
# The arguments are passed as a dictionary and these arguments make a dictionary inside function with name same as the parameter excluding double asterisk **

def fruit(**kwargs):

    print(kwargs)
    print(type(kwargs))

    if 'fruit' in kwargs:
        print("My fruit of choice is: {}".format(kwargs['fruit']))

    else:
        print("I did not find any fruit.")


fruit(fruit= 'apple', desert= 'pudding')


# Output
"""
{'fruit': 'apple', 'desert': 'pudding'}
<class 'dict'>
My fruit of choice is: apple
"""





print("\n")





# Combining *args and **kwargs

print("\n========== Combining *args and **kwargs ==========\n")

def combine(*args,**kwargs):

    print(args)

    print(kwargs)

    print("I would like {} {}.".format(args[0], kwargs['food']))


combine(10,20,30, fruit="orange", food="eggs", animal= "dog")


# Output
"""
(10, 20, 30)
{'fruit': 'orange', 'food': 'eggs', 'animal': 'dog'}
I would like 10 eggs.
"""