# Creating a decorator function

def new_decorator(original_func):


    def wrap_func():

        print("Some extra functionality before original function.")

        original_func()

        print("Some extra functionality after original function.")

    
    return wrap_func



# Without @ operator (Manual Decorator)

def func_needs_decorator():
    print("I want to be decorated!!")


decorated_func= new_decorator(func_needs_decorator)

decorated_func()



# Output
"""
Some extra functionality before original function.
I want to be decorated!!
Some extra functionality after original function.
"""


print("\n")


# Using @ operator (Actual Decorator)

@new_decorator
def need_decorator():
    print("I also need a decorator!!")


need_decorator()


# Output
'''
Some extra functionality before original function.
I also need a decorator!!
Some extra functionality after original function.
'''



print("\n")




# We can comment our decorator any time if we do not need the extra functionality.

#@new_decorator
func_needs_decorator()


# Output
"""
I want to be decorated!!
"""