# List indexing

print("\n========== List Indexing ==========\n")

l1=[1,2,3,4]
print(l1[2]) # 3
print(l1[-3]) # 2

l2= [1,2,3,4,['hello']]
print(l2[-1])           # ['hello']
print(l2[-1][0])        # hello

# Reassign 'hello' in this nested list to say 'goodbye' instead
list3 = [1,2,[3,4,'hello']]
list3[-1][-1] = 'goodbye'
print(list3)                #  [1,2,[3,4,'goodbye']]

# =============================================================

# List Slicing

print("\n========== List Slicing ==========\n")

my_list = ['one','two','three',4,5]

# Grab element at index 0
print(my_list[0])

# Grab index 1 and everything past it
print(my_list[1:])

# Grab everything UP TO index 3
print(my_list[:3])


# Output
# one
# ['two', 'three', 4, 5]
# ['one', 'two', 'three']