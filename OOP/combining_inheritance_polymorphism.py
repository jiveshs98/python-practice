# Combining Inheritance and Polymorphism




# Note: Abstract class is a class which is never meant to be instantiated (No instance is created). It is always used as a base class. The methods of this class are called abstract methods.

# ABSTRACT CLASS
class Animal():
    
    def __init__(self, name):
        self.name= name

    def speak(self):
        raise NotImplementedError("Subclass must be used to implement this abstract method.")


# Derived Class
class Dog(Animal):

    def __init__(self, name):
        self.name= name

    def speak(self):
        return self.name + " says WOOF!"


# Derived Class
class Cat(Animal):

    def __init__(self, name):
        self.name= name

    def speak(self):
        return self.name + " says MEOW!"



"""
myanimal= Animal('fred')
myanimal.speak()                # NotImplementedError with the given message
"""


krypto= Dog("Krypto")
isis= Cat("Isis")

print(krypto.speak())
print(isis.speak())

# Output
"""
Krypto says WOOF!
Isis says MEOW!
"""