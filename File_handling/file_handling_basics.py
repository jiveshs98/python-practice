# Reading a file using python

with open("files/samples.txt", encoding="utf-8") as f:
    content= f.read()

print(content)

# Output

"""
apple
mango
orange
"""




print("\n\n")




# Writing a file in python

numbers= [1,2,3]

with open("files/numbers.txt",'w',encoding="utf-8") as f:
    for i in numbers:
        f.write(str(i) + "\n")





# Reading and writing file in python

with open("files/test.txt", mode= "w", encoding= "utf-8") as f:
    f.write("My first file\n")
    f.write("This file\n\n")
    f.write("contains three lines\n")


with open("files/test.txt", mode= "r", encoding="utf-8") as myfile:
    content= myfile.read()

print(content)



print("\n")



# Delete a file using Python
# To avoid errors, we will check a file if it exists before deleting

import os

if os.path.exists("this.text"):
    os.remove("test.txt")

else:
    print("The file does not exist.")




# Delete a folder using Python
# We can only remove empty folders

import os

os.rmdir("files/my_folder")