# Python follows the LEGB Rule

# L --> Local variable
# E --> Enclosing Function locals
# G --> Global
# B --> Built-in names


# Example of local scope
f= lambda x : x**2
"""
print(x)                    # NameError: name 'x' is not defined
"""


# Example of Global and Enclosing Function Scope

name = 'This is a global name'

def greet():
    
    # This is an Enclosing function variable
    name = 'Sam'
    
    def hello():
        print('Hello '+name)
    
    hello()

greet()                 # Hello Sam

print(name)             # This is a global name




# Example of Built-in Scope
print(len)                          # <built-in function len>