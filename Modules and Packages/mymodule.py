# This is a module
# Module is a .py script that can be run into another .py script
# Package is a collection of modules
# A package folder needs to have __init__.py file.

def my_func():
    print("I am in mymodule.")