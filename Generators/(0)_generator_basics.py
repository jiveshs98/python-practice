"""
Generator functions allow us to write a function that can send back a value and then later resume to pick up where it left off. This type of function is a generator in Python, allowing us to generate a sequence of values over time. The main difference in syntax will be the use of a yield statement.

In most aspects, a generator function will appear very similar to a normal function. The main difference is when a generator function is compiled they become an object that supports an iteration protocol. 

That means when they are called in your code they don't actually return a value and then exit. Instead, generator functions will automatically suspend and resume their execution and state around the last point of value generation. 

The main advantage here is that instead of having to compute an entire series of values up front, the generator computes one value and then suspends its activity awaiting the next instruction. This feature is known as state suspension.

For example, the range() function does not produce a list in memory for all the values from start to stop. Instead, it just keeps track of the last number and the step size, to provide a flow of numbers which is lot more efficient. Now if a user did need a list, they have to transform the generator to a list with list(range(0,10)).



Use case for a generator:

If the output has the potential of taking up a large amount of memory and you only intend to iterate through it, you would want to use a generator.
"""



# Example (Without Generator)

def create_cubes(n):

    result= []

    for x in range(n):
        result.append(x**3)

    return result


for i in create_cubes(5):
    print(i)


# Output
"""
0
1
8
27
64
"""

# Note that if we pass a big number, like 1000000, then it will store the list in memory and take a lot of space.



print('\n')



# Example 2 (Using Generators)

def cubes(n):

    for x in range(n):
        yield x**3


for i in cubes(5):
    print(i)



# Output
"""
0
1
8
27
64
"""


# This code produces the same result and is a lot more memory efficient.

# We can generate a list if we want.
print(list(cubes(5)))                   # [0, 1, 8, 27, 64]