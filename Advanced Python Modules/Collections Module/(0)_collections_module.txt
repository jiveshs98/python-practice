Collections Module

The collections module is a built-in module that implements specialized container data types providing alternatives to Python’s general purpose built-in containers.


We have implemented 4 subclasses of the collections module:-

        1. counter
        2. defaultdict
        3. OrderedDict
        4. namedtuple