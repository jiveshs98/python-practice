# Implementing filter function


# The filter function returns an iterator yielding those items of iterable for which function(item) is true. 

# Meaning you need to filter by a function that returns either True or False. Then passing that into filter (along with your iterable) and you will get back only the results that would return True when passed to the function.

# Syntax ---> filter(function, iterable)

# Parameters
# function ---> A Function to be run for each item in the iterable
# iterable ---> The iterable to be filtered



def check_even(num):
    return num%2 == 0

nums= [1,2,3,4,5,6]


print(filter(check_even,nums))          # <filter object at 0x014F81D8>


print("\n")


print(list(filter(check_even,nums)))        # [2, 4, 6]


print("\n")


for x in filter(check_even,nums):
    print(x)


# Output
"""
2
4
6
"""