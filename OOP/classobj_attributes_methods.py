# Class Object Attributes and Methods

# Example 1

class Dog():

    # Class Object Attributes
    # Same for any instance of a class

    species= 'Mammal'


    # Constructor for user-defined attributes
    def __init__(self, breed, name):

        self.breed= breed
        self.name= name


    # Methods ----> Operations/Actions
    def bark(self, number):
        print("WOOF! My name is {} and the number is {}.".format(self.name, number))



mydog= Dog(breed='Pug', name="Edgar")

print(mydog.name)           # Edgar
print(mydog.breed)          # Pug

# Even though we didn't define species for mydog object, it was automatically assigned as it was a class object attribute.
print(mydog.species)        # Mammal


mydog.bark(69)                # WOOF! My name is Edgar and the number is 69.




print("\n")




class Circle():

    # Class Object Attribute
    pi= 3.14

    # Constructor
    def __init__(self, radius=1):

        self.radius= radius
        self.area= self.pi * radius * radius

    # Method
    def circumference(self):

        # Note: We can also refer self.pi as Circle.pi to show that it is a class object attribute.

        return 2 * Circle.pi * self.radius



mycircle= Circle()
print(mycircle.radius)          # 1
print(mycircle.pi)              # 3.14

mycircle2= Circle(30)
print(mycircle2.radius)             # 30
print(mycircle2.pi)                 # 3.14
print(mycircle2.circumference())    # 188.4
print(mycircle2.area)               # 2826.0