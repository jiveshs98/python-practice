print("\n========== Instantiate(Create) Tuple ==========\n")

# create an empty tuple
py_tuple = ()
print("A blank tuple:", py_tuple)


print('\n')


# create a tuple without using round brackets
py_tuple = 33, 55, 77
print("A tuple set without parenthesis:", py_tuple, "type:", type(py_tuple))


print('\n')


# create a tuple of numbers
py_tuple = (33, 55, 77)
print("A tuple of numbers:", py_tuple)


print('\n')


# create a tuple of mixed numbers
# such as integer, float, imaginary
py_tuple = (33, 3.3, 3+3j)
print("A tuple of mixed numbers:", py_tuple)


print('\n')


# create a tuple of mixed data types
# such as numbers, strings, lists
py_tuple = (33, "33", [3, 3])
print("A tuple of mixed data types:", py_tuple)


print('\n')


# create a tuple of tuples
# i.e. a nested tuple
py_tuple = (('x', 'y', 'z'), ('X', 'Y', 'Z'))
print("A tuple of tuples:", py_tuple)




# Tuple Constructor

print("\n========== Tuple Constructor ==========\n")

py_tuple = tuple(("apple", "banana", "cherry")) # note the double round-brackets
print(type(py_tuple))           # <class 'tuple'>
print(py_tuple)                 # ('apple', 'banana', 'cherry')

# creating a tuple from a set
py_tuple = tuple({33, 55 , 77})
print(type(py_tuple))           # <class 'tuple'>
print(py_tuple)                 # (33, 77, 55)

# creating a tuple from a list
py_tuple = tuple([33, 55 , 77])
print(type(py_tuple))           # <class 'tuple'>
print(py_tuple)                 # (33, 55, 77)




# Creating a tuple of size one

print("\n========== Creating a tuple of size one ==========\n")

# This will create a string instead of a tuple
py_tuple = ('single')
print(type(py_tuple))           # <class 'str'>


# This will create an integer instead of a tuple
py_tuple = (4)
print(type(py_tuple))           # <class 'int'>


# You need to place a comma after the first element to create a tuple of size "one"
py_tuple = ('single',)
print(type(py_tuple))           # <class 'tuple'>

py_tuple = (4,)
print(type(py_tuple))           # <class 'tuple'>


# You can use a list of one element and convert it to a tuple
py_tuple = tuple(['single'])
print(type(py_tuple))           # <class 'tuple'>


# You can use a set of one element and convert it to a tuple
py_tuple = tuple({'single'})
print(type(py_tuple))           # <class 'tuple'>