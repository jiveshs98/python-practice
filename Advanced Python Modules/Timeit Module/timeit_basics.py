"""
Sometimes it's important to know how long your code is taking to run, or at least know if a particular line of code is slowing down your entire project. Python has a built-in timing module to do this.

This module provides a simple way to time small bits of Python code. 

It has both a Command-Line Interface as well as a callable one. 

It avoids a number of common traps for measuring execution times.
"""




x= "-".join(str(n) for n in range(10))

print(x)


"""
0-1-2-3-4-5-6-7-8-9
"""


from timeit import timeit

# Comparing time for three methods (in sec)

res= timeit("'-'.join(str(n) for n in range(10))", number= 10000)

print(res)          # 0.130870144


res2= timeit("'-'.join([str(n) for n in range(10)])", number= 10000)

print(res2)         # 0.077250926

res3= timeit("'-'.join(map(str,range(10)))", number= 10000)

print(res3)         #0.053522060999999954