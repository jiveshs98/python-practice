"""
We use 3 keywords for exception handling:-

    1. try: This is the block of code to be attempted (May contain some errors).

    2. except: This block of code will be executed in case there is an error in try block.

    3. finally: A final block of code to be executed, regardless of an error.



---> A single try statement can have multiple except statements depending on the requirement. In this case, a try block contains statements that can throw different types of exceptions.

---> We can also add a generic except clause which can handle all possible types of exceptions.

---> We can even include an else clause after the except clause. The instructions in the else block will execute if the code in the try block doesn’t raise an exception.
"""




# try-except block

print("\n========== try-except ==========\n")

try:
    add= 10 + '10'

except:
    print("Wrong Addition!!")


# Output
# Wrong Addition!!







# try-except-else block

print("\n========== try-except-else ==========\n")

try:
    add= 10 + 10

except:
    print("Hey it looks like you are not adding correctly!")

else:
    print("Addition went well")
    print(add)


# Output
"""
Addition went well
20
"""






# try-except-finally block

print("\n========== try-except-finally ==========\n")


# Example 1

try:
    print("Hello World")

except:
    print("There is some error!!")

finally:
    print("I always run")



# Output
"""
Hello World
I always run
"""





# Example 2

try:
    f= open("testfile","r")
    f.write("Write a test line.")

except TypeError:
    print("There was a type error!!")

except OSError:
    print("Hey you have an OS error!!")

finally:
    print("I always run")


# Output
"""
Hey you have an OS error!!
I always run
"""





# Exception handling in function

print("\n========== Exception Handling in Function ==========\n")

def ask_int():

    while True:

        print("\n")

        try:
            num= int(input("Please provide a number: "))

        except:
            print("Whoops!! That is not a number.")
            continue

        else:
            print("Yes, thank you!")
            break

        finally:
            print("End of try-except-finally.")
            print("I will always run at the end!")


        

ask_int()


# Output
"""
Please provide a number: q
Whoops!! That is not a number.
End of try-except-finally.
I will always run at the end!


Please provide a number: ok
Whoops!! That is not a number.
End of try-except-finally.
I will always run at the end!


Please provide a number: 20
Yes, thank you!
End of try-except-finally.
I will always run at the end!
"""