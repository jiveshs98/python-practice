# List Methods

print("\n========== List Methods ==========\n")

#1. append() -> Adds an element at the end of the list.

print("\n********** Append **********\n")

fruits= ["apple","banana","cherry"]
fruits.append("orange")
print(fruits)           # ['apple', 'banana', 'cherry', 'orange']

a= [1,2,3,4]
b=["a","b","c"]
a.append(b)
print(a)                # [1, 2, 3, 4, ['a', 'b', 'c']]




#2. extend() -> Adds the elements of a list(or any iterable) to the end of current list

print("\n********** Extend **********\n")

a= [1,2,3,4]
b= ['a','b','c','d']
a.extend(b)
print(a)                    # [1, 2, 3, 4, 'a', 'b', 'c', 'd']

a= [1,2,3,4]
b= (1,2,3,4)
a.extend(b)
print(a)                    # [1, 2, 3, 4, 1, 2, 3, 4]




#3. pop() -> Removes the element at the specified position
#         -> If the index is not specified, then it removes the last item
#         -> It returns the removed value

print("\n********** Pop **********\n")

lst= [1,2,3,4]
x= lst.pop(2)
print(x)                    # 3
print(lst)                  # [1, 2, 4]


lst= ["apple","banana","cherry"]
print(lst.pop())                    # cherry
print(lst)                          # ['apple', 'banana']




#4. remove() -> Removes the first occurrence of the element with the specified value
#            -> Returns None

print("\n********** Remove **********\n")

lst= ['a','e','i','o','u']
lst.remove('i')
print(lst)                          # ['a', 'e', 'o', 'u']




#5. sort() -> Sorts the list in ascending order, by default
#          -> To sort in descending order, sort(reverse=True)

print("\n********** Sort (Ascending) **********\n")

lst= [11,2,63,4]
lst.sort()
print(lst)                          # [2, 4, 11, 63]


lst=['Ford','BMW','Chevrolet']
lst.sort()
print(lst)                          # ['BMW', 'Chevrolet', 'Ford']




print("\n********** Sort (Descending) **********\n")

lst= [11,2,63,4]
lst.sort(reverse= True)
print(lst)                          # [63, 11, 4, 2]


lst=['Ford','BMW','Chevrolet']
lst.sort(reverse= True)
print(lst)                          # ['Ford', 'Chevrolet', 'BMW']




#6. reverse() -> Reverses the order of the list

print("\n********** Reverse **********\n")

lst=[1,2,3,'a','b','c']
lst.reverse()
print(lst)                      # ['c', 'b', 'a', 3, 2, 1]




#7. clear() -> Removes all the elements from the list.

print("\n********** Clear **********\n")

lst= ["abc","def","ghi"]
lst.clear()
print(lst)                      # []




#8. copy() -> Returns a copy of list

print("\n********** Copy **********\n")

lst= ['a','e','i','o','u']
x= lst.copy()
print(x)                        # ['a', 'e', 'i', 'o', 'u']




#9. count() -> Returns the no. of times a value has appeared in the list

print("\n********** Count **********\n")

points= [1,4,2,9,2,3,4]

x= points.count(9)
y= points.count(2)
z= points.count(10)

print(x)                        # 1
print(y)                        # 2
print(z)                        # 0




#10. index() -> Returns the index of the first occurrence of the specified value.

print("\n********** Index **********\n")

fruits= ['apple','banana','cherry']
x= fruits.index('cherry')
print(x)                                # 2


fruits= [4,55,64,32,16,32]
x= fruits.index(32)
print(x)                                # 3




#11. insert() -> Adds a specified element at the specified position

print("\n********** Insert **********\n")

fruits= ['apple','banana','cherry']
fruits.insert(1,"orange")
print(fruits)                               # ['apple', 'orange', 'banana', 'cherry']