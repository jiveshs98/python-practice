s= "This is a string"

# Length of string
print(len(s))

# Output: 16

# =================================================

# String Indexing

# First element
print(s[0])

# Output: T

#Last Element
print(s[len(s)-1])
print(s[-1])

# Output
# g
# g

# =================================================

# String Slicing

#Everything
print(s[:])

# Grab everything but the last letter
print(s[:-1])

# Grab everything, but go in steps size of 1
print(s[::1])

# Grab everything, but go in step sizes of 2
print(s[::2])

# We can use this to print a string backwards
print(s[::-1])


# Output
# This is a string
# This is a strin
# This is a string
# Ti sasrn
# gnirts a si sihT

# ===================================================

# String Operators

#1. Concatenation(+)
# It combines two strings into one

var1 = 'Python'
var2 = 'String'
print (var1+var2)

# Output: PythonString


#2 Repetition(*)
# This operator creates a new string by repeating it a given number of times.

var1 = 'Python'
print (var1*3)
# Output: PythonPythonPython


#3 Membership (in)
# This operator returns ‘True’ value if the character is present in the given String.

var1 = 'Python'
print ('n' in var1)
# Output: True

#4 Membership (not in)
# It returns ‘True’ value if the character is not present in the given String.

var1 = 'Python'
print ('N' not in var1)
# Output: True

# =====================================================

# String Iteration
# Using 'for', we can iterate through all the characters of a string.

for var in var1: 
    print (var,end="")
# Output: Python

# =====================================================

# Raw String (r/R)
#We can use it to ignore the actual meaning of Escape characters inside a string. For this, we add ‘r’ or ‘R’ in front of the String.

print (r'\n')
# Output: \n

print (R'\n')
# Output: \n

