# Implementing Polymorphism

class Dog():

    def __init__(self, name):
        self.name= name

    def speak(self):
        return self.name + " says WOOF!"


class Cat():

    def __init__(self, name):
        self.name= name

    def speak(self):
        return self.name + " says MEOW!"



pluto = Dog("Pluto")
felix = Cat("Felix")


# Same function speak() is unique for different objects.
print(pluto.speak())
print(felix.speak())

# Output
"""
Pluto says WOOF!
Felix says MEOW!
"""




print("\n")





# Showing Polymorphism using for loop
for pet in [pluto, felix]:

    print(type(pet))
    print(pet.speak())


# Output
"""
<class '__main__.Dog'>
Pluto says WOOF!
<class '__main__.Cat'>
Felix says MEOW!
"""




print("\n")





# Showing Polymorphism using function
def pet_speak(pet):
    print(pet.speak())


pet_speak(pluto)
pet_speak(felix)

# Output
"""
Pluto says WOOF!
Felix says MEOW!
"""