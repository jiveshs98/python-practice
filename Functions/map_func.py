# Implementing map function


# The map() function executes a specified function for each item in a iterable. The item is sent to the function as a parameter.

# Syntax --> map(function, iterables)

# Parameters:
# function ---> Required. The function to execute for each item
# iterable ---> Required. A sequence, collection or an iterator object. You can send as many iterables as you like, just make sure the function has one parameter for each iterable.


# Example 1

def square(num):
    return num**2


nums = [1,2,3,4]

print(map(square,nums))         # <map object at 0x032682C8>

print("\n")

for x in map(square,nums):
    print(x)

# Output
"""
1
4
9
16
"""

print("\n")

print(list(map(square,nums)))       # [1, 4, 9, 16]



print("\n")



# Example 2

def splicer(mystring):
    if len(mystring)%2 == 0:
        return 'EVEN'
    else:
        return mystring[0]


names= ['Jivesh','Kapil','Akshay']

print(list(map(splicer,names)))     # ['EVEN', 'K', 'EVEN']