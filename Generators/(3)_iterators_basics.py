"""
We know that strings are iterable.
"""

s = 'hello'

#Iterate over string
for let in s:
    print(let)


# Output
"""
h
e
l
l
o
"""

print("\n")


"""
But this does not mean that a string is itself an iterator.
"""
try:
    print(next(s))

except TypeError:
    print("TypeError: 'str' object is not an iterator")



print("\n")



"""
This means that a string object supports iteration, but we can not directly iterate over it as we could with a generator function.

The iter() function allows us to do just that!
"""


s_iter = iter(s)

print(next(s_iter))     # h

print(next(s_iter))     # e

print(next(s_iter))     # l